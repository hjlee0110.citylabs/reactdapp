# 1. 메타마스크 설치 (웹 - 크롬에서 확장 프로그램으로 추가)
https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn?hl=ko
#### **[BSC 테스트 네트워크 - 메타마스크에 추가하는 방법]**<br/>
https://growingsaja.tistory.com/728 <br/>
해당 블로그에서 "5. 입력칸에 정보 입력 [테스트넷]" 부터 따라하시면 됩니다. 

------------------------------------------------------
위의 블로그에서 제공하는 RPC URL로 연결이 되지 않는 경우, 아래의 RPC URL을 사용하면 됩니다.
#### **[RPC URL]** <br/>
https://data-seed-prebsc-1-s3.binance.org:8545

#### **[블록 탐색기 URL (옵션)]** <br/>
https://testnet.bscscan.com

# 2. 프로젝트 실행
#### $ yarn <br/>
#### $ yarn start <br/>

# 3. 프로젝트 설명
https://github.com/MetaMask/test-dapp <br/>
모든 내용은 위의 Metamask test-dapp 프로젝트를 기반으로 함.

## [페이지 별 설명]
#### 1) localhost:3000/ <br/>
test-dapp 프로젝트에서 가장 기본이 되는 내용만 정리하여 해당 프로젝트에 정리함.

#### 2) localhot:3000/join <br/>
test-dapp 프로젝트에서 회원가입 시 사용할 수 있는 method를 가지고 간단한 예시 형태로 구현함.