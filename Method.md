## [MetaMask Docs] - Method 
### https://docs.metamask.io/guide/rpc-api.html

--------------------------------
<br/>

### 1. wallet_requestPermissions 
지갑 접근 권한을 얻을 수 있음 <br/>

### 2. eth_requestAccounts 
이미 사이트와 메타마스크 지갑이 연결 되어있는 경우 <br/>
-> 접근 권한이 있는 경우로 즉시 지갑 주소를 배열 형태로 리턴함. <br/><br/>
사이트에 처음 접속하여 메타마스크 지갑이 연결되어 있지 않은 경우 <br/>
-> 접근 권한을 요청하여 사용자가 승인하면 지갑 주소를 배열 형태로 리턴함. 

### 3. eth_getBalance 
메타마스크 지갑 주소를 params로 전달하여 현재 잔액을 알 수 있음.

### 4. personal_sign 
사용자가 "서명" 할시 어떤 행위에 대한 동의를 얻을 수 있음. <br/>
-> 회원가입시 많이 사용된다고 공식문서에서 언급하고 있음.

### 5. eth_sendTransaction
지갑주소를 통해 A -> B 로 토큰을 전송 할 수 있음. <br/>
-> 해당 프로젝트에서는 optional 한 요청 params (gasPrice 등...)를 제외하고 기본 기능을 보여줄 수 있도록 <br/>
to, from, amount 만 사용하여 구현하였음.


<br/><br/>
해당 프로젝트는 기본적인 method만 사용하고 있으며 추가적인 method는 상단의 MetaMask Docs 에서 확인이 가능함.


