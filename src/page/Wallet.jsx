import React from "react";
import { useState } from "react";
import metamaskFox from "../metamask-fox.svg";

// 동작 컴포넌트 hooks
import Network from "../components/NetworkAndChain";
import GetBalance from "../components/GetBalance";
import Connect from "../components/Connect";
import Permissions from "../components/Permissions";
import SendFrom from "../components/SendForm";
import PersonalSign from "../components/PersonalSign";

function Wallet() {
  const [defaultAccount, setDefaultAccount] = useState("");

  return (
    <div>
      <header>
        <div id="logo-container">
          <h1 id="logo-text" className="text-center">
            E2E Test Dapp
          </h1>

          <img id="mm-logo" alt="metamaskFox" src={metamaskFox} />
        </div>
      </header>
      <section>
        <h3 className="card-title">Status</h3>
        <div className="row">
          <Network />
          <div className="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
            <p className="info-text alert alert-success">
              Accounts: <span id="accounts">{defaultAccount}</span>
            </p>
          </div>
          <GetBalance defaultAccount={defaultAccount} />
        </div>
      </section>
      <section>
        <div className="row d-flex justify-content-center">
          <div className="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
            <div className="card">
              <div className="card-body">
                <h4 className="card-title">Basic Actions</h4>
                <Connect
                  defaultAccount={defaultAccount}
                  setDefaultAccount={setDefaultAccount}
                />
              </div>
            </div>
          </div>
        </div>
      </section>
      <PersonalSign defaultAccount={defaultAccount} />
      <section>
        <div className="row d-flex justify-content-center">
          <div className="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
            <div className="card">
              <Permissions />
            </div>
          </div>
        </div>
      </section>
      <section>
        <div className="row d-flex justify-content-center">
          <div className="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
            <div className="card">
              <SendFrom defaultAccount={defaultAccount} />
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default Wallet;
