import React, { useState, useRef, useEffect } from "react";
import metamaskFox from "../metamask-fox.svg";
import { useInput } from "../hooks/useInput";
import { Buffer } from "buffer";

// Opensea 및 NFT 마켓 Metamask 지갑 연결 과정 참고함

/*
# 회원가입 시 사용한 method
1. eth_requestAccounts
2. personal_sign
*/

const Join = () => {
  const [myAccount, setMyAccount] = useState("");
  const [myData, setMyData] = useState({});

  const id = useInput("");
  const pw = useInput("");
  const name = useInput("");

  const onSubmit = (e) => {
    e.preventDefault();
    setMyData({
      id: id.value,
      pw: pw.value,
      name: name.value,
    });
  };

  const getSign = async (account) => {
    const joinMsg =
      "MTT 플랫폼에 오신걸 환영합니다! \n MTT 플랫폼의 가입 약관에 동의하신다면 서명해주세요! \n 가입약관: https://### ";
    try {
      const from = account[0];
      const msg = `0x${Buffer.from(joinMsg, "utf8").toString("hex")}`;
      const sign = await window.ethereum.request({
        method: "personal_sign",
        params: [msg, from, "Example password"],
      });
      if (sign) {
        setMyAccount(account[0]);
      }
    } catch (err) {
      console.error(err);
    }
  };

  const connectMetamask = () => {
    if (window.ethereum && window.ethereum.isMetaMask) {
      window.ethereum
        .request({ method: "eth_requestAccounts" })
        .then((account) => {
          getSign(account);
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      console.log("Need to install MetaMask");
    }
  };

  return (
    <div className="container join-contianer">
      <form onSubmit={onSubmit}>
        <div className="input-group input-group-lg">
          <span className="input-group-text w200" id="inputGroup-sizing-lg">
            아이디
          </span>
          <input
            type="text"
            className="form-control"
            aria-label="Sizing example input"
            aria-describedby="inputGroup-sizing-lg"
            {...id}
          />
        </div>
        <div className="input-group input-group-lg">
          <span className="input-group-text w200" id="inputGroup-sizing-lg">
            비밀번호
          </span>
          <input
            type="password"
            className="form-control"
            aria-label="Sizing example input"
            aria-describedby="inputGroup-sizing-lg"
            {...pw}
          />
        </div>
        <div className="input-group input-group-lg">
          <span className="input-group-text w200" id="inputGroup-sizing-lg">
            이름
          </span>
          <input
            type="text"
            className="form-control"
            aria-label="Sizing example input"
            aria-describedby="inputGroup-sizing-lg"
            {...name}
          />
        </div>
        <div className="input-group input-group-lg">
          <span className="input-group-text w200" id="inputGroup-sizing-lg">
            내 메타마스크 지갑 주소
          </span>
          <input
            type="text"
            className="form-control"
            aria-label="Sizing example input"
            aria-describedby="inputGroup-sizing-lg"
            value={myAccount}
            readOnly
          />
        </div>
        <div className="button-box">
          <button
            type="button"
            className="btn btn-outline-primary join-btn"
            data-mdb-ripple-color="dark"
            onClick={connectMetamask}
          >
            메타마스크 연결
            <img
              className="metamask-connect-btn"
              alt="metamaskFox"
              src={metamaskFox}
            />
          </button>
          <button
            type="submit"
            className="btn btn-primary join-btn"
            disabled={myData && myAccount ? false : true}
          >
            가입
          </button>
        </div>
      </form>
    </div>
  );
};

export default Join;
