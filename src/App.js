import "./App.css";
import { Web3ReactProvider } from "@web3-react/core";
import { Web3Provider } from "@ethersproject/providers";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import Wallet from "./page/Wallet";
import Join from "./page/Join";


function App() {
  const getLibrary = (provider) => {
    return new Web3Provider(provider);
  };
  return (
    <Web3ReactProvider getLibrary={getLibrary}>
      <Router>
        <Routes>
          <Route path="/" exact={true} element={<Wallet />} />
          <Route path="/join" exact={true} element={<Join />} />
        </Routes>
      </Router>
    </Web3ReactProvider>
  );
}

export default App;
