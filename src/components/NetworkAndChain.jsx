import React from "react";
import { useState, useEffect } from "react";

const Network = () => {
  const [networkID, setNetworkID] = useState("");
  const [chainID, setChainID] = useState("");

  const handleNewNetwork = (networkId, chainId) => {
    setNetworkID(networkId);
    setChainID(chainId);
  };

  const getNetworkAndChainId = async () => {
    if (window.ethereum && window.ethereum.isMetaMask) {
      try {
        const networkId = await window.ethereum.request({
          method: "net_version",
        });
        const chainId = await window.ethereum.request({
          method: "eth_chainId",
        });
        handleNewNetwork(networkId, chainId);
      } catch (err) {
        console.error(err);
      }
    } else {
      // 메타마스크 설치 페이지로 이동
    }
  };

  useEffect(() => {
    getNetworkAndChainId();
  });
  return (
    <>
      <div className="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
        <p className="info-text alert alert-primary">
          Network: <span id="network">{networkID}</span>
        </p>
      </div>

      <div className="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
        <p className="info-text alert alert-secondary">
          ChainId: <span id="chainId">{chainID}</span>
        </p>
      </div>
    </>
  );
};

export default Network;
