//Eventually, the personal_sign spec (opens new window)was proposed, which added a prefix to the data so it could not impersonate transactions. We also made this method able to display human readable text when UTF-8 encoded, making it a popular choice for site logins.
import React, { useEffect, useState } from "react";
import { Buffer } from "buffer";

const PersonalSign = ({ defaultAccount }) => {
  const [signResult, setSignResult] = useState("");
  /**
   * Personal Sign
   */
  const personalSign = async () => {
    const exampleMessage = "Example `personal_sign` message";
    if (window.ethereum && window.ethereum.isMetaMask) {
      try {
        const from = defaultAccount;
        const msg = `0x${Buffer.from(exampleMessage, "utf8").toString("hex")}`;
        const sign = await window.ethereum.request({
          method: "personal_sign",
          params: [msg, from, "Example password"],
        });
        setSignResult(sign);
      } catch (err) {
        console.error(err);
      }
    } else {
      // 메타마스크 설치 페이지로 이동
    }
  };

  return (
    <section>
      <div className="row d-flex justify-content-center">
        <div className="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
          <div className="card">
            <div className="card-body">
              <h4>Personal Sign</h4>

              <button
                className="btn btn-primary btn-lg btn-block mb-3"
                id="personalSign"
                onClick={personalSign}
              >
                Sign
              </button>

              <p className="info-text alert alert-warning">
                Result: <span id="personalSignResult">{signResult}</span>
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default PersonalSign;
