import React, { useEffect } from "react";

const Connect = ({ defaultAccount, setDefaultAccount }) => {
  const connectWalletHandler = async () => {
    if (window.ethereum && window.ethereum.isMetaMask) {
      await window.ethereum
        .request({ method: "eth_requestAccounts" })
        .then((result) => {
          setDefaultAccount(result[0]);
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      // 메타마스크 설치 페이지로 이동
      console.log("Need to install MetaMask");
    }
  };
  return (
    <>
      {defaultAccount ? (
        <button
          className="btn btn-primary btn-lg btn-block mb-3"
          id="connectButton"
          disabled
        >
          Connected
        </button>
      ) : (
        <button
          className="btn btn-primary btn-lg btn-block mb-3"
          id="connectButton"
          onClick={connectWalletHandler}
        >
          Connect
        </button>
      )}

      <button
        className="btn btn-primary btn-lg btn-block mb-3"
        id="getAccounts"
      >
        eth_accounts
      </button>

      <p className="info-text alert alert-secondary">
        eth_accounts result:{" "}
        <span id="getAccountsResult">{defaultAccount}</span>
      </p>
    </>
  );
};

export default Connect;
