import React from "react";
import { useState, useEffect } from "react";

const Permissions = () => {
  const [permissionStauts, setPermissionStatus] = useState("");

  const requestPermissions = async () => {
    if (window.ethereum && window.ethereum.isMetaMask) {
      try {
        const permissionsArray = await window.ethereum.request({
          method: "wallet_requestPermissions",
          params: [{ eth_accounts: {} }],
        });
        let result = getPermissionsDisplayString(permissionsArray);
        setPermissionStatus(result);
      } catch (err) {
        console.error(err);
      }
    } else {
      // 메타마스크 설치 페이지로 이동
    }
  };

  const getPermissions = async () => {
    if (window.ethereum && window.ethereum.isMetaMask) {
      try {
        const permissionsArray = await window.ethereum.request({
          method: "wallet_getPermissions",
        });
        let result = getPermissionsDisplayString(permissionsArray);
        setPermissionStatus(result);
      } catch (err) {
        console.error(err);
      }
    } else {
      // 메타마스크 설치 페이지로 이동
    }
  };

  function getPermissionsDisplayString(permissionsArray) {
    if (permissionsArray.length === 0) {
      return "No permissions found.";
    }
    const permissionNames = permissionsArray.map(
      (perm) => perm.parentCapability
    );
    return permissionNames
      .reduce((acc, name) => `${acc}${name}, `, "")
      .replace(/, $/u, "");
  }

  return (
    <div className="card-body">
      <h4 className="card-title">Permissions Actions</h4>

      <button
        className="btn btn-primary btn-lg btn-block mb-3"
        id="requestPermissions"
        onClick={requestPermissions}
      >
        Request Permissions
      </button>

      <button
        className="btn btn-primary btn-lg btn-block mb-3"
        id="getPermissions"
        onClick={getPermissions}
      >
        Get Permissions
      </button>

      <p className="info-text alert alert-secondary">
        Permissions result:{" "}
        <span id="permissionsResult">{permissionStauts}</span>
      </p>
    </div>
  );
};

export default Permissions;
