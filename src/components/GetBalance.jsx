import React, { useState } from "react";
import { useEffect } from "react";

const GetBalance = ({ defaultAccount }) => {
  const [balance, setBalance] = useState("");

  // 잔액 확인
  const getBalance = async (address) => {
    if (window.ethereum && window.ethereum.isMetaMask) {
      try {
        await window.ethereum
          .request({
            method: "eth_getBalance",
            params: [address, "latest"],
          })
          .then((balance) => {
            const wei = parseInt(balance, 16);
            const eth = wei / Math.pow(10, 18); // parse to ETH
            setBalance(eth);
          });
      } catch (error) {
        console.error(error);
      }
    } else {
      // 메타마스크 설치 페이지로 이동
    }
  };

  useEffect(() => {
    if (defaultAccount) {
      getBalance(defaultAccount);
    }
  }, [defaultAccount]);

  return (
    <div className="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
      <p className="info-text alert alert-success">
        Your Balance: <span id="balance">{balance}</span>
      </p>
    </div>
  );
};
export default GetBalance;
