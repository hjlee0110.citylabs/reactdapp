import React, { useEffect, useState } from "react";
import Web3 from "web3";
import { useInput } from "../hooks/useInput";

const web3 = new Web3("https://data-seed-prebsc-1-s1.binance.org:8545");

// to : 0x48CB99dAC38704b782A1c772DC64BC82BEef774d

const SendFrom = ({ defaultAccount }) => {
  const receiver = useInput("");
  const amount = useInput("");

  const submitFormButton = async () => {
    let params = [
      {
        from: defaultAccount,
        to: receiver.value,
        value: web3.utils.toHex(web3.utils.toWei(amount.value)),
      },
    ];

    if (window.ethereum && window.ethereum.isMetaMask) {
      await window.ethereum.request({
        method: "eth_sendTransaction",
        params,
      });
    } else {
      // 메타마스크 설치 페이지로 이동
    }
  };

  return (
    <div className="card-body">
      <h4 className="card-title">Send form</h4>
      <div className="form-group">
        <label>From</label>
        <input
          className="form-control"
          type="text"
          id="fromInput"
          value={defaultAccount}
          readOnly
        />
      </div>
      <div className="form-group">
        <label>To</label>
        <input
          className="form-control"
          type="text"
          id="toInput"
          {...receiver}
        />
      </div>
      <div className="form-group">
        <label>Amount</label>
        <input
          className="form-control"
          type="text"
          id="amountInput"
          {...amount}
        />
      </div>
      <button
        onClick={submitFormButton}
        className="btn btn-primary btn-lg btn-block mb-3"
        id="submitForm"
      >
        Submit
      </button>
    </div>
  );
};

export default SendFrom;
